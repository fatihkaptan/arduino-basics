#include <dht11.h>
#define sensor 8 //digital pin'e tak 
dht11 DHT11; //obje atıyoruz
void setup() {
  Serial.begin(9600);
  }

void loop() {
  int readData = DHT11.read(sensor); //hangi pin'den okuyacağını belirtiyoruz
  float nem = DHT11.humidity;  //lib'den humidity,temp kullanıyoruz
  float temp = DHT11.temperature;
  Serial.print("Sicaklik (Celcius): ");
  Serial.println(temp);
  Serial.print("Nem (%): ");
  Serial.println(nem);
  Serial.println("----------");
  delay(300);
}
