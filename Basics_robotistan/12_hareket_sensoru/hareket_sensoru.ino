#include <Servo.h>

int sensor =  8;
int servo = 9;
int hareket;

Servo motor;
void setup() {
  motor.attach(servo);
  pinMode(sensor,INPUT);
}

void loop() {
  hareket = digitalRead(sensor);
  if(hareket==HIGH){ 
    //el sallama hareketi
    motor.write(150);
    delay(250);
    motor.write(30);
    delay(250);
    motor.write(150);
    delay(250);
    motor.write(30);
    delay(250);
    motor.write(150);
    delay(250);
    motor.write(30);
    delay(250);
    motor.write(90);
  }
  else{
    motor.write(90);
  }

}
