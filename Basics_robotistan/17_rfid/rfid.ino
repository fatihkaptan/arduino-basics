#include <MFRC522.h> //rfid kütüpahnesii
#include <MFRC522Extended.h>

#include <SPI.h>
#include <Servo.h>

int RST_PIN = 9;
int SS_PIN = 10;
int ServoPin = 8;

Servo motor;
MFRC522 rfid(SS_PIN,RST_PIN); //NESNE TANIMLAMA
byte ID[4] = {183,93 ,177 ,78}; //0-255 arası tutan 8bitlik deger, int ise 2 byte 16 bittir


void setup() {

  motor.attach(ServoPin);
  Serial.begin(9600);
  SPI.begin(); //ardu ile rfid kart haberleşmesi
  rfid.PCD_Init(); //rfid başlatma init
  

}

void loop() {

  if(! rfid.PICC_IsNewCardPresent()) //ünlem tersini alır, yeni kart okutulamdığında return eder
  return;
  if(! rfid.PICC_ReadCardSerial()) //kart okutulmadığında gereksiz dönmemesi için not ekliyoruz yine
  return;
  if( rfid.uid.uidByte[0] == ID[0] &&
  rfid.uid.uidByte[1] == ID[1] &&
  rfid.uid.uidByte[2] == ID[2] &&
  rfid.uid.uidByte[3] == ID[3]){
    //if içinde tanımladığımız şart 4 byte değerini de ve (&&) operatoryle karşıltırıyor
    Serial.println("Kapı Açıldı1");
    ekranaYazdir();
    motor.write(180);
    delay(3000);
    motor.write(0);
    delay(1000);    //motor hareketleri tanımlanır
  }

  else{ //hatalı kart bloğu
    Serial.print("Yetkisiz Kart!  >>  ");
    ekranaYazdir();
  }
rfid.PICC_HaltA(); //yeni kart okuayan kadar durur sürekli yazırmayı keser
Serial.println(" ");
}

void ekranaYazdir() {
  Serial.print("ID Numarası: ");
  for(int sayac=0;sayac<4;sayac++){
    Serial.print(rfid.uid.uidByte[sayac]);
    Serial.print(" ");
  }
  
}
