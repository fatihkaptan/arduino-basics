#define pot A0
#define led 3

void setup() {
  //analog işlemlerde pinMode gerek yoktur.

}

void loop() {
  int deger = analogRead(pot);
  deger = deger/4; //analogdan 10bit okunuyor, onu 8bit'e dönüştürüyoruz
//  deger = map(deger,0,1023,0,255); //bir başka çözüm şekli
  analogWrite(led,deger); //analogwrite 8 bit şeklindedir.
}
