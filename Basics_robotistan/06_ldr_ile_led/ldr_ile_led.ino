#define led 3
#define ldr A0

void setup() {
  pinMode(led,OUTPUT),
  Serial.begin(9600); //analog okumak için setup kısmında serial monitörü başlatıyoruz.
}

void loop() {
  int isik = analogRead(ldr);
  Serial.println(isik);
  //analogdan okuduğumuz değeri değerlendirerek şart yapılarıyla istediğimiz uygulamayı yapabilriiz
  if(isik<=880){
    digitalWrite(led,HIGH);
  }
  else if(isik>=900){
    digitalWrite(led,LOW);
  }
  else{
    digitalWrite(led,HIGH);
    delay(50);
    digitalWrite(led,LOW);
    delay(50);    
  }

  //burada seraaldan okuduğum değeri değerlendirerek 880'den küçükse flaş kapalı, led açık
  //900'den büyükse flaş açık,led kapalı
  // ara değerde ise belirsizlik var, led yanıp-sönüyor
  
  
  delay(50);
  
  
  
}
