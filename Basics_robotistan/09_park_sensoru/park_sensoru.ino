
#define echoPin 6
#define trigPin 7
#define buzzerPin 8

int maximumRange = 50; //hc-sr04 0-400 cm arası algılayabiliyor
int minimumRange = 0;

void setup() {
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(buzzerPin, OUTPUT);

}

void loop() {

  int olcum = mesafe(maximumRange, minimumRange);
  melodi(olcum*10);

}

int mesafe(int maxrange, int minrange)
{
  long duration, distance;

  digitalWrite(trigPin,LOW); 
  delayMicroseconds(2); //ms değil mikro saniye 1/10
  digitalWrite(trigPin, HIGH);//ses dalgası gönderiyoruz
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH); //geçen süreyi hesaplayan fonskiyon
  distance = duration / 58.2; 
  delay(50);

  if(distance >= maxrange || distance <= minrange)
  return 0; //if bloğu sadece bu satır (kırlangıçsız)
  return distance; //burası else gibi düşünebiliriz
}

int melodi(int dly)//dly aslında olcum*10 yukarıda tanımlı mesafe olcum fonskiyonundan cıkıyor
{
  tone(buzzerPin, 440); //440 tonlardan bir tanesi
  delay(dly);
  noTone(buzzerPin);
  delay(dly);
  
}
