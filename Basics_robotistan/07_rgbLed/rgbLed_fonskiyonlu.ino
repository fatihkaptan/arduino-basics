#define led_r 11
#define led_g 10
#define led_b  9

#define pot_r A2
#define pot_g A1
#define pot_b A0

//global degiskenleri tanımlama
int red_value;
int green_value; 
int blue_value; 


void setup() {
  Serial.begin(9600);
  pinMode(led_r,OUTPUT);
  pinMode(led_g,OUTPUT);
  pinMode(led_g,OUTPUT);
//led ayağı belirleme fonskiyonu
}
void ayak_bul(){
  
//aşağıda hangi rengin hangi pine takılı olduğunu görmek için birine 0 diğer ikisine 1 gönderiyoruz
//bunun sebebi rgb led pozitif ucu ardunyodan alıp, negtafi uçları pwm slotlarından bağlı olduğu için
//aslında led'e LOW yani '0' verdiğimizde o renk çalışıyor..;

  //  digitalWrite(led_r,HIGH);
  //  digitalWrite(led_g,HIGH);
  //  digitalWrite(led_g,HIGH);
  
//not : sadece bir kere çalıştırılarak tespit ettikten sonra kullanılması gerekmez

}
//deger okuma fonskiyonu
void deger_oku(){  
  red_value = analogRead(pot_r);
  green_value = analogRead(pot_g);
  blue_value = analogRead(pot_b);
//okunan analog 10bit değerleri  8bit değere atama
  red_value = map(red_value,0,1023,0,255);
  green_value = map(green_value,0,1023,0,255);
  blue_value = map(blue_value,0,1023,0,255);  
}

//pwm ile değerleri analog olarak gönderme fonksiyonu
void deger_yaz(){
  analogWrite(led_r,red_value);
  analogWrite(led_b,blue_value);
  analogWrite(led_g,green_value);  
}

void loop() {
//  ayak_bul();
  deger_oku();
  deger_yaz();  
}
