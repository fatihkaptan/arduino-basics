#include <virtuabotixRTC.h>
#include <LiquidCrystal.h>

int CLK_PIN = 6;
int DAT_PIN = 8;
int RST_PIN = 7;

int rs=12, en=11, d4=5, d5=4, d6=3, d7=2; //ekran pin tanılama
LiquidCrystal lcd(rs,en,d4,d5,d6,d7);


virtuabotixRTC myRTC(CLK_PIN,DAT_PIN,RST_PIN);

void setup() {
  Serial.begin(9600);
//  myRTC.setDS1302Time(00, 31, 00,2, 28, 04, 2020); //saat ayarlama (tek seferlik)
//saniye - dakika - saat - haftanın günü(örn salı:2) - gün - ay - yıl

  lcd.begin(16,2);
}

void loop() {
  myRTC.updateTime(); //güncel saati alma

  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(myRTC.dayofmonth);
  lcd.print("/");
  lcd.print(myRTC.month);
  lcd.print("/");
  lcd.print(myRTC.year);
  lcd.setCursor(0,1);
  lcd.print(myRTC.hours);
  lcd.print(":");
  lcd.print(myRTC.minutes);
  lcd.print(":");
  lcd.print(myRTC.seconds);
  //  Serial.print("Tarih / Saat: ");
//  Serial.print(myRTC.dayofmonth);
//  Serial.print("/");
//  Serial.print(myRTC.month);
//  Serial.print("/");
//  Serial.print(myRTC.year);
//  Serial.print("  ");
//  Serial.print(myRTC.hours);
//  Serial.print(":");
//  Serial.print(myRTC.minutes);
//  Serial.print(":");
//  Serial.print(myRTC.seconds);
//  Serial.println(" ");

  
//  //serial ekran kullanım:
//  Serial.print("Tarih / Saat: ");
//  Serial.print(myRTC.dayofmonth);
//  Serial.print("/");
//  Serial.print(myRTC.month);
//  Serial.print("/");
//  Serial.print(myRTC.year);
//  Serial.print("  ");
//  Serial.print(myRTC.hours);
//  Serial.print(":");
//  Serial.print(myRTC.minutes);
//  Serial.print(":");
//  Serial.print(myRTC.seconds);
//  Serial.println(" ");

  delay(1000);
  

}
