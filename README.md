****Robotistan '[Arduino Dersleri ve Projeleri](https://www.youtube.com/playlist?list=PLDRcccSktQd5mfXDtGv975V77RCrW6H7U)' serisi üzerinden gidiyorum.****

![](https://media1.giphy.com/media/kEVmdxc9xOtQn898aM/giphy.gif?cid=ecf05e47f4db7535ca38318bb253a51305df7c8454ba43dd&rid=giphy.gif)

***Proje içerikleri;***
* Ham Kod(*ino)
* Ham Devre Şeması(*fzz) [[fritzing](https://github.com/fritzing/fritzing-app/releases) uygulaması kullanıyorum]
* Devre Şeması(*jpg




    
```
master/Basics_robotistan:
    ├───01_blink
    ├───02_butonLed
    ├───03_analogOkuma_potans
    ├───04_potLed_kontrol
    ├───05_karasimsek
    ├───06_ldr_ile_led
    ├───07_rgbLed
    ├───08_sensor_dht11
    ├───09_park_sensoru
    ├───10_ses_sensoru
    ├───11_ldr_ile_servo_kontrol
    ├───12_hareket_sensoru
    ├───13_lcd_mesafeOlcer
    ├───14_rtc_saat
    ├───15_joystik_kontrol
    └───16_transistorTetik
```

   